//
//  ViewController.m
//  CIDemoApp
//
//  Created by anu Priya on 5/4/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)handleButtonClick:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Hello world" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alert show];
    NSLog(@"second test");
    
}

@end
